# ISEM Server

Main purpose: 
* Gather information of all ISEMS at one place that can be managed by the ISEM community
* Offer a ready to use toolbox for lecturers who don't want to implement everything again.
* Standardize structure to make it easier to use
* Knowledge database of how to host the ISEM
* Reuse login data (no creation of a new user and login)
* Social network

## Features:

* Webspace for hosting the ISEM announcements and lecture notes
* Archive of past ISEMs
* Forum

## Scenarios of Usage

### Use Case A

Lecturer wants to host self-responsibly 
* only link-forwarding of `YY.isem.org` and `YY.isem.org/forum` to the respective phases

### Use Case B

Lecturer wants to publish the notes but has her own Forum:

* Use Case A for the forum
* Use a GitLab Repository for uploading the notes. Pipeline delivers static webcontent to `YY.isem.org`

### Use Case C

Lecture does not want to host anything
* Use a GitLab Repository for uploading the notes. Pipeline delivers static webcontent to `YY.isem.org`
* Use the forum server

## Possible Implementation

1 or 2 servers for hosting the forum and the archive 
Each isem lecturer needs to create a GitLab Repository with a respective pipeline (by template)-> to automate the process of publishing and updating the notes
Forum Software:
* discourse (very nice but only virtualization with docker)
* other software

# Docker

To run serve the webpage locally run
```bash
docker run -it --rm -p 8080:80 -v `pwd`:/var/www/html php:7-apache
```
